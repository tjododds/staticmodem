#include <stdio.h>

int factorial(int start, int end){
	if (start > end){
		return -1;
	}
	else{
		int value = end;
		if (start != end){
			value = value * factorial(start, end - 1);
		}
		return value;
	}
		
}
int main (void) {
	printf("5! = %d \n", factorial(1,5));
	return 0;
}
